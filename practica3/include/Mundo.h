// VICTOR CAVERO HA MODIFICADO ESTE ARCHIVO



// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"

#define MAX_BUFFER 1024
#define MAX_PUNTOS 3//puntos maximos de la partida

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	DatosMemCompartida memCompartida;
	DatosMemCompartida *memoria;
	int fdBot;//descriptor de fichero de bot
	
	int control2;//indica cuando se debe borrar el tiempo
	float tiempo;//tiempo para controlar el jugador 2
	
	int puntos1;
	int puntos2;
	int fd;//identifiacador tuberia
	char buf[MAX_BUFFER];
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
