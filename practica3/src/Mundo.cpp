// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	sprintf(buf,"FIN DEL PROGRAMA");
	write(fd,buf,sizeof(buf));
        close(fd);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{		
	//el siguiente if es para manejar el fin del programa cuando se llegue al maximo de puntos
	if(puntos1<MAX_PUNTOS && puntos2< MAX_PUNTOS)
	{
		jugador1.Mueve(0.025f);
		jugador2.Mueve(0.025f);
		esfera.Mueve(0.025f);
	
		//actualizamos los datos de memoria compartida
		memoria->raqueta=jugador1;
		memoria->esfera=esfera;

		//a continuacion implementamos el control del bot para el jugador 1	
		if(memoria->accion==1)
			OnKeyboardDown('w',0,0);
		if(memoria->accion==-1)
			OnKeyboardDown('s',0,0);
		
		int i;
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(esfera);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}

		jugador1.Rebota(esfera);
		jugador2.Rebota(esfera);
		if(fondo_izq.Rebota(esfera))
		{
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
	
			sprintf(buf,"Jugador 2 anota un punto, lleva %d puntos\n", puntos2);
			write(fd,buf,sizeof(buf));
		}

		if(fondo_dcho.Rebota(esfera))
		{
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;

			sprintf(buf,"jugador 1 anota un punto, lleva %d puntos\n",puntos1);
			write(fd,buf,sizeof(buf));
		}
	}
	else
	{
		sprintf(buf,"FIN DEL PROGRAMA");
		write(fd,buf,sizeof(buf));
		close(fd);
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	
	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//codigo relativo a la FIFO
	fd=open("/tmp/mififo",O_WRONLY);
	if(fd<1) 
		perror("ERROR AL ABRIR EL FIFO EN MUNDO\n");	

	//codigo relativo a la memoria compartida para el bot
	fdBot=open("/tmp/bot",O_RDWR);
	if(fdBot==-1)
		printf("ERROR AL ABRIR EL FICHERO DEL BOT EN MUNDO\n");
	write(fdBot,&memCompartida,sizeof(memCompartida));
	memoria=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fdBot,0);
	if(memoria<0)
		printf("ERROR AL PROYECTAR LA MEMORIA\n");
	close(fdBot);
}
