#include "DatosMemCompartida.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>

int main (void)
{
	DatosMemCompartida *memoria;
	int fdBot;
	
	fdBot=open("/tmp/bot",O_CREAT|O_RDWR,0700);
	if(fdBot<0)
		printf("ERROR AL ABRIR EL FICHERO DEL BOT EN EL BOT\n");
	memoria=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fdBot,0);
	if(memoria<0)
		printf("ERROR EN EL MMAP DEL BOT\n");
	
	while(1)
	{
		//acciones del jugador 1
		if(memoria->raqueta.y1 < memoria->esfera.centro.y)
			memoria->accion=1;
		else if(memoria->raqueta.y1 > memoria->esfera.centro.y)
			memoria->accion=-1;
		else
			memoria->accion=0;
		
		//dormimos el bot durante 25ms
		usleep(2500);
	}
	close(fdBot);
	munmap(memoria,sizeof(DatosMemCompartida));
	return 0;
}

