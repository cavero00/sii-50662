#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define MAX_BUFFER 1024

int main()
{
        int fd;
        char buf[MAX_BUFFER];

	mkfifo("/tmp/mififo",0666);
	fd=open("/tmp/mififo",O_RDONLY);
	if(fd<0)
		perror("ERROR AL ABRIR LA FIFO EN EL LOGGER\n");
	
	while(1)
	{
		read(fd,buf,sizeof(buf));
		printf("%s\n",buf);
		
		if(strcmp(buf,"FIN DEL PROGRAMA")==0)
			break;
	}
	close(fd);
	unlink("/temp/mififo");
	return 0;
}	
